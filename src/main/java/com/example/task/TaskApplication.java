package com.example.task;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class TaskApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(TaskApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        String text = "Salam,     necesiz   dostlar?";
//        String[] words = text.split(" ");
//
//        for (String s : words) {
//            if (!s.isBlank()) {
//                if (!s.endsWith(",") && !s.endsWith("?"))
//                    System.out.println(s);
//            }
//        }
//        String text = "salam,";
//        String[] split = text.split(",");
//        System.out.println(split[0]);
//        System.out.println(split[1]);
    }
}
