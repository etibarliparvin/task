package com.example.task.controller;

import com.example.task.dto.request.TextRequest;
import com.example.task.dto.response.GeneralResponse;
import com.example.task.dto.response.TextResponse;
import com.example.task.service.TextService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/wordstat")
public class TextController {

    private final TextService textService;

    @PostMapping
    public ResponseEntity<GeneralResponse<?>> wordstat(@RequestBody TextRequest request) {
        TextResponse response = textService.save(request);
        return ResponseEntity.ok(new GeneralResponse<>(HttpStatus.OK.toString(), response));
    }
}
