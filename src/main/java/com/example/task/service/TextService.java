package com.example.task.service;

import com.example.task.dto.request.TextRequest;
import com.example.task.dto.response.TextResponse;

public interface TextService {

    TextResponse save(TextRequest request);
}
