package com.example.task.service.impl;

import com.example.task.dto.request.TextRequest;
import com.example.task.dto.response.Result;
import com.example.task.dto.response.TextResponse;
import com.example.task.entity.Text;
import com.example.task.mapper.TextMapper;
import com.example.task.repository.TextRepository;
import com.example.task.service.TextService;
import com.example.task.utils.ArrayUtils;
import com.example.task.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TextServiceImpl implements TextService {

    private final TextRepository textRepository;
    private final TextMapper mapper;

    @Override
    public TextResponse save(TextRequest request) {
        Result result = StringUtils.countOfSpecialSymbolsAndWords(request.content());
        List<String> words = result.getWords();
        words.sort(Comparator.reverseOrder());
        List<String> list = ArrayUtils.eliminateRepeatedElements(words);
        Text text = mapper.toEntity(request, result, list.toString());
        textRepository.save(text);
        TextResponse response;
        if (!list.isEmpty()) {
            response = mapper.toResponse(result.getCountOfWords(), list.subList(0, 5));
        }
        response = mapper.toResponse(result.getCountOfWords(), list);
        System.out.println(list);
        return response;
    }
}
