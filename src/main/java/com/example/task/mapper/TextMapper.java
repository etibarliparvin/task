package com.example.task.mapper;

import com.example.task.dto.request.TextRequest;
import com.example.task.dto.response.Result;
import com.example.task.dto.response.TextResponse;
import com.example.task.entity.Text;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TextMapper {

    Text toEntity(TextRequest request, Result result, String mostFrequentWords);

    @Mappings({
            @Mapping(source = "countOfWords", target = "wordsCount"),
            @Mapping(source = "list", target = "mostFrequentWords")
    })
    TextResponse toResponse(int countOfWords, List<String> list);
}
