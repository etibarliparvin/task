package com.example.task.utils;

import com.example.task.dto.response.Result;

import java.util.ArrayList;
import java.util.List;

public class StringUtils {

    public static Result countOfSpecialSymbolsAndWords(String text) {
        int countOfSymbols = 0;
        int countOfWords = 0;
        List<String> list = new ArrayList<>();
        String[] words = text.split(" ");
        for (String s : words) {
            if (!s.isBlank()) {
                if (s.startsWith(",") || s.startsWith(".")
                        || s.startsWith("?") || s.startsWith(":")
                        || s.startsWith("-") || s.startsWith("_")) {
                    countOfSymbols++;
                } else {
                    if (s.endsWith(",") || s.endsWith(".")
                            || s.endsWith("?") || s.endsWith(":")
                            || s.endsWith("-") || s.endsWith("_")) {
                        countOfSymbols++;
                    }
                    countOfWords++;
                    list.add(s);
                }
            }
        }
        Result result = new Result();
        result.setCountOfSymbols(countOfSymbols);
        result.setCountOfWords(countOfWords);
        result.setWords(list);
        return result;
//        return Result.builder()
//                .countOfSymbols(countOfSymbols)
//                .countOfWords(countOfWords)
//                .words(list)
//                .build();
    }
}
