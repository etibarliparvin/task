package com.example.task.utils;

import java.util.ArrayList;
import java.util.List;

public class ArrayUtils {

    public static List<String> eliminateRepeatedElements(List<? extends String> list) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < list.size() - 1; i++) {
            if (!list.get(i).equals(list.get(i + 1)))
                result.add(list.get(i));
        }
        result.add(list.get(list.size() - 1));
        return result;
    }
}
