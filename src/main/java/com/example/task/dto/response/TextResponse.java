package com.example.task.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class TextResponse {
    private int wordsCount;
    private List<String> mostFrequentWords;
}
