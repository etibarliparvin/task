package com.example.task.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class Result {
    private int countOfSymbols;
    private int countOfWords;
    private List<String> words;
}
