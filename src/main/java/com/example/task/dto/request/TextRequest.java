package com.example.task.dto.request;

public record TextRequest(String content) {
}
