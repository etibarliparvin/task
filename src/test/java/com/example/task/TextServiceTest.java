package com.example.task;

import com.example.task.dto.request.TextRequest;
import com.example.task.dto.response.Result;
import com.example.task.dto.response.TextResponse;
import com.example.task.entity.Text;
import com.example.task.mapper.TextMapper;
import com.example.task.repository.TextRepository;
import com.example.task.service.impl.TextServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class TextServiceTest {

    @InjectMocks
    public TextServiceImpl textService;
    @Mock
    private TextRepository textRepository;
    @Mock
    private TextMapper textMapper;

    private TextRequest request;
    private TextResponse response;
    private Text text;
    private Result result;
    private String mostFrequentWords;
    private static final int COUNT_OF_WORDS = 9;
    private static final int COUNT_OF_SPECIAL_SYMBOLS = 0;
    private static final List<String> MOST_FREQUENT_WORDS =
            List.of("the", "quick", "over", "lazy", "jumps");
    private static String CONTENT = "the quick brown fox jumps over the lazy dog";

    @BeforeEach
    public void setUp() {
        request = new TextRequest(CONTENT);

        response = new TextResponse();
        response.setWordsCount(COUNT_OF_WORDS);
        response.setMostFrequentWords(MOST_FREQUENT_WORDS);

        text = new Text();
        text.setContent(CONTENT);
        text.setCountOfSymbols(COUNT_OF_SPECIAL_SYMBOLS);
        text.setCountOfWords(COUNT_OF_WORDS);
        text.setMostFrequentWords("[the, quick, over, lazy, jumps, fox, dog, brown]");

        result = new Result();
        result.setCountOfSymbols(COUNT_OF_SPECIAL_SYMBOLS);
        result.setCountOfWords(COUNT_OF_WORDS);
        result.setWords(List.of("the", "the", "quick", "over", "lazy", "jumps", "fox", "dog", "brown"));

        mostFrequentWords = "[the, quick, over, lazy, jumps, fox, dog, brown]";
    }

    @Test
    public void whenTextRequestThenTextResponse() {
        // arrange
        Mockito.when(textMapper.toEntity(request, result, mostFrequentWords)).thenReturn(text);
        Mockito.when(textMapper.toResponse(COUNT_OF_WORDS, MOST_FREQUENT_WORDS)).thenReturn(response);
        Mockito.when(textRepository.save(text)).thenReturn(text);

        // act
        TextResponse textResponse = textService.save(request);

        // assert
        Assertions.assertThat(textResponse.getWordsCount()).isEqualTo(response.getWordsCount());

        Mockito.verify(textMapper, Mockito.times(1))
                .toResponse(ArgumentMatchers.any(), ArgumentMatchers.any());
        Mockito.verify(textMapper, Mockito.times(1))
                .toEntity(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
        Mockito.verify(textRepository, Mockito.times(1)).save(text);
    }
}
